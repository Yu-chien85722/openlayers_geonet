import Swal from "sweetalert2";

function alertError(erroMessage) {
  Swal.fire({
    title: "Oops..",
    text: erroMessage,
    icon: "error",
  });
}

function alertSuccess(successMessage) {
  Swal.fire({
    text: successMessage,
    icon: "success",
    showConfirmButton: false,
    timer: 2000,
  });
}

function alertWithTwoBtn(title, text, position, cb) {
  Swal.fire({
    position: position,
    title: title,
    text: text,
    icon: "question",
    allowOutsideClick: false,
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "Yes",
  }).then((result) => {
    if (result.isConfirmed) {
      cb();
    }
  });
}

const alertWithInput = (inputTitle, inputPlaceHolder, validate) => {
  const swalOptions = {
    html: `
			<h3>${inputTitle}</h3>
			`,
    input: "text",
    position: "top-end",
    inputPlaceholder: inputPlaceHolder,
    inputValidator: (value) => {
      if (!value) {
        return validate.nullValueMessage;
      } else if (!validate.regex.test(value)) {
        return validate.errorMessage;
      }
    },
    showCancelButton: true,
    confirmButtonText: "OK",
    cancelButtonText: "Cancel",
    allowOutsideClick: false,
    allowEscapeKey: false,
  };

  return Swal.fire(swalOptions);
};

export { alertError, alertSuccess, alertWithTwoBtn, alertWithInput };
