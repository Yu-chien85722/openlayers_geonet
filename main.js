import "./style.css";
import { Map, View } from "ol";
import Draw from "ol/interaction/Draw.js";
import TileLayer from "ol/layer/Tile";
import VectorLayer from "ol/layer/Vector";
import { OSM, XYZ, BingMaps, Vector as VectorSource } from "ol/source";
import { Point, LineString, Polygon, Circle } from "ol/geom";
import Feature from "ol/Feature.js";
import {
  alertSuccess,
  alertError,
  alertWithInput,
  alertWithTwoBtn,
} from "./alert";

// vector layer
const vectorSource = new VectorSource({ wrapX: false });
const vector = new VectorLayer({
  source: vectorSource,
  // set geometry style
  style: {
    "fill-color": "rgba(255, 255, 255, 0.2)",
    "stroke-color": "#F9F900",
    "stroke-width": 3,
    "circle-radius": 7,
    "circle-fill-color": "#F9F900",
  },
});

// map sources
const mapSources = {
  openSourceMap: new OSM(),
  bingMap: new BingMaps({
    // key from https://www.bingmapsportal.com/
    key: "Ak_-D2mFwMCJmynHIKLqEitwxPiggv3OgUamSflDyo1SVnDfffT0qrf2U2oqgf-W",
    imagerySet: "RoadOnDemand",
  }),
  googleMap: new XYZ({
    url: "http://mt0.google.com/vt/lyrs=m&hl=en&x={x}&y={y}&z={z}",
  }),
};

// map layers 圖層
const mapLayers = [];
for (const mapSource in mapSources) {
  const tileLayer = new TileLayer({
    properties: { type: mapSource },
    visible: false,
    preload: Infinity,
    source: mapSources[mapSource],
  });
  mapLayers.push(tileLayer);
}

const map = new Map({
  target: "map",
  layers: [...mapLayers, vector],
  view: new View({
    center: [0, 0],
    zoom: 5,
  }),
});

// change source map feat
const sourceSelect = document.getElementById("source-select");
function changeHandler() {
  const selectedValue = sourceSelect.value;
  // console.log(selectedValue);
  for (const mapLayer of mapLayers) {
    // console.log(mapLayer.getProperties().type);
    mapLayer.setVisible(mapLayer.getProperties().type === selectedValue);
  }
}
sourceSelect.addEventListener("change", changeHandler);
changeHandler();

// draw feat
const geometrySelect = document.getElementById("geometry-select");
let draw;
const postRegion = (data) => {
  // console.log("postData", data);
  return fetch("http://localhost:3001/api/saveGeometry", {
    mode: "cors",
    cache: "no-cache",
    credentials: "include",
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: data,
  }).then((res) => {
    if (res.status === 200) {
      return res.json();
    } else {
      throw new Error(res.status);
    }
  });
};
function drawendHandler(e, drawType) {
  // get geometry values ( coordinates, radius )
  const coordinates =
    e.feature.getGeometry().getCoordinates() ??
    e.feature.getGeometry().getCenter();
  // console.log(coordinates);
  let radius = "";
  if (drawType === "Circle") {
    radius = e.feature.getGeometry().getRadius();
    // console.log(radius);
  }
  // Alert - save geometry or not
  alertWithTwoBtn(
    "Do you want to save this geometry ?",
    "You won't be able to revert this!",
    "top-end",
    () => {
      // let user named the geometry
      const validate = {
        regex: /^[a-zA-Z0-9_\u4e00-\u9fa5-]{1,100}$/,
        errorMessage:
          "Please fill in a valid name, no spaces or special characters allowed.",
        nullValueMessage: "please name this geometry",
      };
      alertWithInput(
        "please name this geometry",
        "ex: Aa中文0-9-_",
        validate
      ).then((result) => {
        if (result.isConfirmed) {
          // console.log(result.value);
          // post to BK
          const data = {
            name: result.value,
            type: drawType,
            coordinates: coordinates,
            radius: radius,
          };
          postRegion(JSON.stringify(data))
            .then((result) => {
              if (result.status) {
                alertSuccess(result.data);
                // re-render stored regions modal
                renderAreaList();
              }
            })
            .catch((err) => {
              alertError(`can't save this geometry, erro: ${err.message}`);
            });
        }
      });
    }
  );
}
function addInteraction() {
  const geometryType = geometrySelect.value;
  if (geometryType !== "None") {
    draw = new Draw({
      source: vectorSource,
      type: geometryType,
    });
    map.addInteraction(draw);
    draw.on("drawend", (e) => {
      drawendHandler(e, geometryType);
    });
  }
}
function geoChangeHandler() {
  map.removeInteraction(draw);
  addInteraction();
}
geometrySelect.addEventListener("change", geoChangeHandler);
addInteraction();

// clear all geometry
const clearStrokeBtn = document.getElementById("clear-stroke");
clearStrokeBtn.addEventListener("click", () => {
  vector.getSource().clear();
});

// show stored area feat
const openModalBtn = document.getElementById("open-modal");
const closeModalBtn = document.getElementById("close-modal");
const storedModal = document.getElementById("stored-modal");
const areaList = document.getElementById("area-list");
const getStoredRegions = () => {
  return fetch("http://localhost:3001/api/getGeometry", {
    mode: "cors",
    cache: "no-cache",
    credentials: "include",
    method: "GET",
  }).then((res) => {
    if (res.status === 200) {
      return res.json();
    } else {
      throw new Error(res.status);
    }
  });
};
function showRegion(e) {
  // console.log(e.currentTarget.id);
  // console.log(e.currentTarget.dataset.type);
  const target = e.currentTarget;
  const type = target.dataset.type;
  const coordinates = JSON.parse(target.dataset.coordinates); // [[100, 100]]
  // console.log(coordinates);
  // console.log(typeof coordinates);
  const radius = Number(target.dataset.radius);

  // check if it has been drawn
  // console.log(vectorSource.getFeatures()[0].getId());
  let drawnFeature = vectorSource.getFeatureById(target.id);
  // draw new geometry
  if (!drawnFeature) {
    switch (type) {
      case "Point":
        drawnFeature = new Feature({
          geometry: new Point(...coordinates),
        });
        break;
      case "LineString":
        drawnFeature = new Feature({
          geometry: new LineString(coordinates),
        });
        break;
      case "Polygon":
        drawnFeature = new Feature({
          geometry: new Polygon([coordinates]),
        });
        break;
      case "Circle":
        drawnFeature = new Feature({
          geometry: new Circle(...coordinates, radius),
        });
        break;
      default:
        break;
    }
    drawnFeature.setId(target.id);
    vectorSource.addFeatures([drawnFeature]);
  }

  // locate the geometry
  if (type === "Point") {
    map.getView().setCenter(...coordinates);
  } else {
    map
      .getView()
      .fit(drawnFeature.getGeometry(), { padding: [250, 250, 250, 250] });
  }
  closeModalBtn.click();
}
const createAreaListHtml = (result) => {
  let html = "<div>Don't have any regions.</div>";
  if (result.length > 0) {
    html = result
      .map((region) => {
        const coordinatesStringfy = JSON.stringify(region.coordinates);
        return `
       <div id=${region.id} class="flex gap-2 border-2 rounded-lg p-3 w-full">
         <div>${region.name}</div>
         <div class="cursor-pointer map-location" id=${"loc-" + region.id}
          data-type=${
            region.type
          } data-coordinates=${coordinatesStringfy} data-radius=${
          region.radius ?? ""
        }
          >
           <i class="fa-solid fa-location-dot fa-xl"></i>
         </div>
       </div>
       `;
      })
      .join("");
  }
  return html;
};
const renderAreaList = () => {
  getStoredRegions()
    .then((result) => {
      // console.log("getRegions", result);
      if (result.status) {
        const areaListHtml = createAreaListHtml(result.data);
        // console.log(areaListHtml);
        areaList.innerHTML = areaListHtml;
        // add click event
        if (result.data.length > 0) {
          const locations = document.getElementsByClassName("map-location");
          // console.log(locations);
          [...locations].forEach((location) => {
            location.addEventListener("click", (e) => {
              showRegion(e);
            });
          });
        }
      }
    })
    .catch((err) => {
      areaList.innerHTML = "get error...";
      alertError(`can't get stored regions, erro: ${err.message}`);
    });
};
openModalBtn.addEventListener("click", () => {
  areaList.innerHTML = "loading...";
  renderAreaList();
  storedModal.classList.remove("invisible");
});
closeModalBtn.addEventListener("click", () => {
  storedModal.classList.add("invisible");
});

// document.getElementById("getFeatures").addEventListener("click", () => {
//   console.log(vectorSource.getFeatures());
// });
