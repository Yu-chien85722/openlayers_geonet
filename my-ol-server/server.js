const express = require("express");
const mysql = require("mysql2");
const app = express();
const PORT = process.env.PORT || 3001;

// Mysql connection pool
const pool = mysql.createPool({
  host: "localhost",
  user: "kelly",
  password: "password",
  database: "pb_blog",
  port: 3306,
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0,
});

// middleware use
app.use(express.json());
// Add headers before the routes are defined
app.use(function (req, res, next) {
  // Website to allow to connect
  res.setHeader("Access-Control-Allow-Origin", "http://localhost:5173");
  // Request methods to allow
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );
  // Request headers to allow
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type"
  );
  // Set to true if need the website to include cookies in the requests sent
  // to the API (e.g. use sessions)
  res.setHeader("Access-Control-Allow-Credentials", true);
  // Pass to next layer of middleware
  next();
});

// GET
app.get("/api/getGeometry", (req, res) => {
  pool.getConnection((err, connection) => {
    if (err) {
      console.error("Error connecting to MySQL:", err.message);
      return res.status(500).json({ status: false, error: err.message });
    }
    console.log("Connected to MySQL database");

    // get data from table
    connection.query(
      "SELECT * FROM ol_map_geometry",
      (err, results, fields) => {
        connection.release();
        if (err) {
          console.error("Error executing query:", err);
          return res.status(500).json({ status: false, error: err.message });
        }
        // console.log('fields', fields); // database col setting
        // console.log('Query results:', results[0].coordinates[0]);
        res.json({ status: true, data: results });
      }
    );
  });
});

// test
// app.get("/api/getGeometry/:id", (req, res) => {
//   res.send(req.params.id);
//   console.log(req.params);
// });

app.get("/", (req, res) => {
  console.log("root");
  res.send("Hello world");
});

// POST
app.post("/api/saveGeometry", (req, res) => {
  const { name, type, coordinates, radius } = req.body;
  // console.log(typeof name, typeof type, typeof coordinates, typeof radius);
  // console.log(name, type, coordinates, radius);
  pool.getConnection((err, connection) => {
    if (err) {
      console.error("Error connecting to MySQL:", err.message);
      return res.status(500).json({ status: false, error: err.message });
    }
    console.log("Connected to MySQL database");

    // sort data
    let toDBCoordinates = coordinates;
    switch (type) {
      case "Point":
        toDBCoordinates = [coordinates];
        break;
      case "Polygon":
        toDBCoordinates = coordinates[0];
        break;
      case "Circle":
        toDBCoordinates = [coordinates];
        break;
      default:
        break;
    }
    const data = {
      name: name,
      type: type,
      coordinates: toDBCoordinates,
      radius: radius === "" ? null : radius,
    };
    // console.log(data);

    // insert data to table
    const insertQuery =
      "INSERT INTO ol_map_geometry (name, type, coordinates, radius) VALUES (?, ?, ?, ?)";
    connection.query(
      insertQuery,
      [data.name, data.type, JSON.stringify(data.coordinates), data.radius],
      (err) => {
        connection.release();
        if (err) {
          console.error("Error executing query:", err);
          return res.status(500).json({ status: false, error: err.message });
        }
        // if insert successfully
        res
          .status(200)
          .json({ status: true, data: "geometry saved successfully" });
      }
    );
  });
});

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});

// after "Ctrl + c", close the connection to mysql
process.on("SIGINT", () => {
  pool.end((err) => {
    if (err) {
      console.error(
        "An error occurred while closing the connection pool: ",
        err.message
      );
    }
    console.log("MySQL pool connection closed.");
    process.exit(); // exit Node.js process
  });
});
